package com.eweasel.takehometest.eventbus;

/**
 * Simple implementation of the top level event subscriber. Felt wrong to expose 'StreamStartEvent' raw to the listeners,
 * as a lifecycle event. Instead provide a callback for it.
 */
public class BusNode {
    protected final EventBus bus;

    public BusNode(EventBus bus) {
        this.bus = bus;
        bus.addSubscriber(StreamStartEvent.class, (context, e) -> onStreamStarted());
    }

    protected void onStreamStarted() {

    }
}
