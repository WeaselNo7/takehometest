package com.eweasel.takehometest.eventbus;

public interface EventSubscriber<E extends StreamEvent> {
    void callback(EventContext context, E e);
}
