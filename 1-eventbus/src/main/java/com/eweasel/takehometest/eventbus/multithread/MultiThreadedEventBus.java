package com.eweasel.takehometest.eventbus.multithread;

import com.eweasel.takehometest.eventbus.*;

import java.util.*;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;

public class MultiThreadedEventBus {
    // TODO RingBuffer would be ideal here, instead of having a queue per client, we would have a single
    // TODO ring buffer and consumers would have consuming positions within that queue. Slow consumers
    // TODO would be a problem, but aren't they always.
    private final ExecutorService dispatcher = Executors.newSingleThreadExecutor();
    private final List<StreamEvent> events = Collections.synchronizedList(new LinkedList<>());

    // TODO same as SingleThreadedEventBus, keeping an open ended event queue.  Allows us to dispatch
    // TODO full event stream to late joiners.  Random feature, not necessarily needed, and leads to
    // TODO memory management problems of course.
    private final AtomicBoolean stopping = new AtomicBoolean(false);
    // TODO Horrible locking mechanism.
    private final List<EventBusClient> clients = new CopyOnWriteArrayList<>();

    private long timeFromEvent(StreamEvent event) {
        // TODO We probably stamp the event on entry into the queue instead of on delivery.
        // TODO So we would need a wrapper for it
        return System.currentTimeMillis();
    }

    public void startBus() {
        // TODO throw exception if started multiple times

        // Our dispatcher thread loops until 'stop' is called.
        dispatcher.submit(() -> {
            while (!stopping.get()) {
                // Capture the current sequence as of now, so we have a consistent loop of work.
                final int currentSequence = events.size();
                for (EventBusClient client : clients) {
                    if (client.lastReceivedSequence < currentSequence) {
                        client.dispatch(sequencesToDispatcherEvents(client.lastReceivedSequence, currentSequence));
                    }
                }
            }
        });
        addEvents(StreamStartEvent.INSTANCE);
    }

    private List<DispatcherEvent> sequencesToDispatcherEvents(int lastReceived, int current) {
        final List<DispatcherEvent> immutableOutput = new ArrayList<>(current - lastReceived);
        for (int i=lastReceived; i<current; i++) {
            final StreamEvent event = events.get(i);
            immutableOutput.add(new DispatcherEvent(timeFromEvent(event), i+1, event));
        }
        return immutableOutput;
    }

    public void stop() {
        stopping.set(true);
    }

    private void addEvents(StreamEvent... event) {
        events.addAll(Arrays.asList(event));
    }

    public EventBusClient newClient() {
        return newClient(false);
    }

    public EventBusClient newConflatingClient() {
        return newClient(true);
    }

    private EventBusClient newClient(boolean conflation) {
        final EventBusClient client = new EventBusClient(conflation);
        clients.add(client);
        return client;
    }

    // Our crappy wrapper on the event, adds context to the event to allow us to keep our event nice
    // and clean and business-specific.  This could later include information about who sent the event, etc.
    private static class DispatcherEvent {
        private final long timestampMs;
        private final int sequence;
        private final StreamEvent event;

        private DispatcherEvent(long timestampMs, int sequence, StreamEvent event) {
            this.timestampMs = timestampMs;
            this.sequence = sequence;
            this.event = event;
        }
    }

    private interface Conflator {
        List<DispatcherEvent> conflate(Queue<DispatcherEvent> events);
    }

    private static class NoOpConflation implements Conflator {
        final List<DispatcherEvent> output = new ArrayList<>();

        @Override
        public List<DispatcherEvent> conflate(Queue<DispatcherEvent> events) {
            output.clear();
            while (events.peek() != null) {
                output.add(events.remove());
            }
            return output;
        }
    }

    private static class LatestOfEachConflation implements Conflator {
        final Map<Class<?>, DispatcherEvent> conflated = new HashMap<>();
        final List<DispatcherEvent> output = new ArrayList<>();

        @Override
        public List<DispatcherEvent> conflate(Queue<DispatcherEvent> events) {
            conflated.clear();
            output.clear();
            while (events.peek() != null) {
                final DispatcherEvent dispatcherEvent = events.remove();
                conflated.put(dispatcherEvent.event.getClass(), dispatcherEvent);
            }
            output.addAll(conflated.values());
            return output;
        }
    }

    /**
     * Each client receives every message.  The client itself tracks subscriptions and conflation concerns.
     */
    public class EventBusClient implements EventBus {
        // TODO We would use the top level RingBuffer, but for now, let's allow a singleThreadedExecutor
        // TODO to manage our thread's queue of work and the thread management.
        private final ExecutorService executor = Executors.newSingleThreadExecutor();
        private final Queue<DispatcherEvent> clientEvents = new ConcurrentLinkedQueue<>();
        private final EventContext context = new EventContext();

        private final Map<Class<?>, List<EventSubscriber<StreamEvent>>> subscriptions = new HashMap<>();
        private final List<EventSubscriber<StreamEvent>> allSubscribers = new ArrayList<>();

        private final Conflator conflator;

        // Can only be accessed by eventbus dispatcher thread
        private int lastReceivedSequence = 0;

        private EventBusClient(boolean conflation) {
            this.conflator = conflation ? new LatestOfEachConflation() : new NoOpConflation();
        }

        @Override
        public void startClient() {
            executor.submit(() -> {
                while(!stopping.get()) {
                    final List<DispatcherEvent> conflatedEvents = conflator.conflate(clientEvents);

                    for (DispatcherEvent dispatcherEvent : conflatedEvents) {
                        context.initialise(dispatcherEvent.timestampMs, dispatcherEvent.sequence);
                        final StreamEvent event = dispatcherEvent.event;
                        final Class<?> eventType = event.getClass();
                        final List<EventSubscriber<StreamEvent>> subscribers = subscriptions.get(eventType);
                        if (subscribers != null) {
                            for (EventSubscriber<StreamEvent> subscriber : subscribers) {
                                // TODO try-catch
                                subscriber.callback(context, event);
                            }
                        }
                        for (EventSubscriber<StreamEvent> subscriber : allSubscribers) {
                            subscriber.callback(context, event);
                        }
                    }
                }
            });
        }

        private void dispatch(List<DispatcherEvent> events) {
            clientEvents.addAll(events);
            if (!events.isEmpty()) {
                lastReceivedSequence = events.get(events.size() - 1).sequence;
            }
        }

        @SuppressWarnings("unchecked")
        @Override
        public <E extends StreamEvent> void addSubscriber(Class<E> eventType, EventSubscriber<E> subscriber) {
            if (subscriptions.containsKey(eventType)) {
                subscriptions.get(eventType).add((EventSubscriber<StreamEvent>) subscriber);
            } else {
                final List<EventSubscriber<StreamEvent>> eventSubscriptions = new ArrayList<>();
                eventSubscriptions.add((EventSubscriber<StreamEvent>) subscriber);
                subscriptions.put(eventType, eventSubscriptions);
            }
        }

        @Override
        public void addAllSubscriber(EventSubscriber<StreamEvent> subscriber) {
            allSubscribers.add(subscriber);
        }

        @Override
        public void publishEvent(StreamEvent... o) {
            addEvents(o);
        }

    }

}
