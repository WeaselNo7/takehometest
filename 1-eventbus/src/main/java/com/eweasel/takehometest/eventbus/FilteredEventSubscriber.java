package com.eweasel.takehometest.eventbus;


public class FilteredEventSubscriber<E extends StreamEvent> implements EventSubscriber<E>  {
    private final EventSubscriber<E> delegate;
    private final EventFilter<E> predicate;

    public FilteredEventSubscriber(EventSubscriber<E> delegate, EventFilter<E> predicate) {
        this.delegate = delegate;
        this.predicate = predicate;
    }

    @Override
    public void callback(EventContext context, E e) {
        if (predicate.allow(context, e)) {
            delegate.callback(context, e);
        }
    }

    public interface EventFilter<E> {
        boolean allow(EventContext context, E event);
    }
}
