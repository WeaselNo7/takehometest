package com.eweasel.takehometest.eventbus;

public class EventContext {
    long currentEventBusTimeMs;
    long currentSequence;

    public void initialise(long currentEventBusTimeMs, long currentSequence) {
        this.currentEventBusTimeMs = currentEventBusTimeMs;
        this.currentSequence = currentSequence;
    }

    public long getCurrentEventBusTimeMs() {
        return currentEventBusTimeMs;
    }

    public long getCurrentSequence() {
        return currentSequence;
    }
}
