package com.eweasel.takehometest.eventbus;

public class StreamStartEvent implements StreamEvent {
    public static final StreamStartEvent INSTANCE = new StreamStartEvent();

    private StreamStartEvent() {
    }

    @Override
    public String toString() {
        return "StartEvent{}";
    }
}
