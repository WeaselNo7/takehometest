package com.eweasel.takehometest.eventbus;

/**
 * Badly named.  This is now a client's interface to the EventBus, rather than the general event bus interface
 * deserves a name like 'EventDispatcher' perhaps.
 */
public interface EventBus {
    void startClient();

    // Replaced Object with StreamEvent, purely for type-safety in the codebase.
    void publishEvent(StreamEvent... o);

    // How would you denote the subscriber?
    <E extends StreamEvent> void addSubscriber(Class<E> clazz, EventSubscriber<E> e);

    // Would you allow clients to filter the events they receive? How would the interface look like?
    // No, I would instead allow the implementation of EventSubscriber to do the filtering.
    // See FilteredEventSubscriber

    // TODO I would restructure all this so that the event-specific parts are a layer on top of the
    // TODO event delivery mechanism. But ran out of time.
    void addAllSubscriber(EventSubscriber<StreamEvent> e);
}