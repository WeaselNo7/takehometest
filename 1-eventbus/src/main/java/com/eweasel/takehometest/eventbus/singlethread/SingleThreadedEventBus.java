package com.eweasel.takehometest.eventbus.singlethread;

import com.eweasel.takehometest.eventbus.*;

import java.util.*;

public class SingleThreadedEventBus implements EventBus {
    // TODO we have no moving window of events, we have all of them!  Obvious practical implications, and overkill
    // TODO for our single-threaded version where we can't really support late joiners.
    // TODO Use a ring buffer instead (an abstraction on top of an array to track size, position, sequence, etc)
    private final List<StreamEvent> events = new ArrayList<>();

    // In our single-threaded world, a context flyweight makes sense.
    private final EventContext context = new EventContext();

    private final Map<Class<?>, List<EventSubscriber<StreamEvent>>> subscriptions = new HashMap<>();
    private final List<EventSubscriber<StreamEvent>> allSubscribers = new ArrayList<>();

    private int lastSequence = 0;

    public void startBus() {
        // TODO throw exception if started multiple times
        publishEvent(StreamStartEvent.INSTANCE);
        triggerEventDelivery();
    }

    @Override
    public void publishEvent(StreamEvent... toPublish) {
        events.addAll(Arrays.asList(toPublish));
    }

    private void triggerEventDelivery() {
        while (events.size() > lastSequence) {
            final StreamEvent event = events.get(lastSequence);
            lastSequence++;
            context.initialise(timeFromEvent(event), lastSequence);
            final List<EventSubscriber<StreamEvent>> subscribers = subscriptions.get(event.getClass());
            if (subscribers != null) {
                for (EventSubscriber<StreamEvent> subscriber : subscribers) {
                    // TODO try-catch
                    subscriber.callback(context, event);
                }
            }
            for (EventSubscriber<StreamEvent> subscriber : allSubscribers) {
                subscriber.callback(context, event);
            }
        }
    }

    private long timeFromEvent(StreamEvent event) {
        // TODO We probably stamp the event on entry into the queue instead of on delivery.
        // TODO So we would need a wrapper for it
        return System.currentTimeMillis();
    }

    @SuppressWarnings("unchecked")
    @Override
    public <E extends StreamEvent> void addSubscriber(Class<E> eventType, EventSubscriber<E> subscriber) {
        if (subscriptions.containsKey(eventType)) {
            subscriptions.get(eventType).add((EventSubscriber<StreamEvent>) subscriber);
        } else {
            final List<EventSubscriber<StreamEvent>> eventSubscriptions = new ArrayList<>();
            eventSubscriptions.add((EventSubscriber<StreamEvent>)subscriber);
            subscriptions.put(eventType, eventSubscriptions);
        }
    }

    @Override
    public void addAllSubscriber(EventSubscriber<StreamEvent> subscriber) {
        allSubscribers.add(subscriber);
    }

    @Override
    public void startClient() {
        // Purposefully empty.  This class acts as the bus itself as well as the client.
    }

}
