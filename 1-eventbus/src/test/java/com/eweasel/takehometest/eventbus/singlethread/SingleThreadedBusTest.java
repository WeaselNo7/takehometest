package com.eweasel.takehometest.eventbus.singlethread;

import com.eweasel.takehometest.eventbus.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class SingleThreadedBusTest {
    private SingleThreadedEventBus bus;
    private TestEventListener busEvents;

    @BeforeEach
    public void setup() {
        bus = new SingleThreadedEventBus();
        busEvents = new TestEventListener(bus);
    }

    @Test
    public void testSinglePingPong() {
        new SingleBurstPinger(bus, 1);
        new SinglePonger(bus);
        bus.startBus();
        busEvents.expect(StreamStartEvent.INSTANCE);

        busEvents.expect(new Ping(1));
        busEvents.expect(new Pong(1));
        busEvents.expectNoMore();
    }

    @Test
    public void testMultipleSinglePingPong() {
        new MultipleSinglePinger(bus, 3);
        new SinglePonger(bus);
        bus.startBus();
        busEvents.expect(StreamStartEvent.INSTANCE);

        busEvents.expect(new Ping(1));
        busEvents.expect(new Pong(1));
        busEvents.expect(new Ping(2));
        busEvents.expect(new Pong(2));
        busEvents.expect(new Ping(3));
        busEvents.expect(new Pong(3));
        busEvents.expectNoMore();
    }

    @Test
    public void testMultiplePairPingPong() {
        new MultiplePairPinger(bus, 6);
        new SinglePonger(bus);
        bus.startBus();
        busEvents.expect(StreamStartEvent.INSTANCE);

        busEvents.expect(new Ping(1));
        busEvents.expect(new Ping(2));
        busEvents.expect(new Pong(1));
        busEvents.expect(new Pong(2));
        busEvents.expect(new Ping(3));
        busEvents.expect(new Ping(4));
        busEvents.expect(new Pong(3));
        busEvents.expect(new Pong(4));
        busEvents.expect(new Ping(5));
        busEvents.expect(new Ping(6));
        busEvents.expect(new Pong(5));
        busEvents.expect(new Pong(6));
        busEvents.expectNoMore();
    }
}
