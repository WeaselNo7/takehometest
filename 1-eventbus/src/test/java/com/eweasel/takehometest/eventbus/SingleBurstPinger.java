package com.eweasel.takehometest.eventbus;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SingleBurstPinger extends BusNode {
    private final Logger logger = LoggerFactory.getLogger(SingleBurstPinger.class.getName());
    private final int pings;

    public SingleBurstPinger(EventBus busClient, int pings) {
        super(busClient);
        this.pings = pings;
        busClient.addSubscriber(Ping.class, this::onPing);
        busClient.addSubscriber(Pong.class, this::onPong);
        busClient.startClient();
    }

    @Override
    protected void onStreamStarted() {
        logger.info("onStarted, sending Ping 1");
        final StreamEvent[] toSend = new Ping[pings];
        for (int i=0; i<pings; i++) {
            toSend[i] = new Ping(i+1);
        }
        bus.publishEvent(toSend);
    }

    private void onPing(EventContext context, Ping ping) {
        logger.info("onPing, saw my own ping: " + ping);
    }

    private void onPong(EventContext context, Pong pong) {
        logger.info("onPong, saw pong: " + pong);
    }
}
