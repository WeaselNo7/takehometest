package com.eweasel.takehometest.eventbus;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SinglePonger extends BusNode {
    private final Logger logger = LoggerFactory.getLogger(SinglePonger.class.getName());

    public SinglePonger(EventBus busClient) {
        super(busClient);
        busClient.addSubscriber(Ping.class, this::onPing);
        busClient.addSubscriber(Pong.class, this::onPong);
        busClient.startClient();
    }

    private void onPing(EventContext context, Ping ping) {
        logger.info("onPing, saw ping " + ping + ", sending pong");
        bus.publishEvent(new Pong(ping.getCounter()));
    }

    private void onPong(EventContext context, Pong pong) {
        logger.info("onPing, saw my own pong: " + pong);
    }
}
