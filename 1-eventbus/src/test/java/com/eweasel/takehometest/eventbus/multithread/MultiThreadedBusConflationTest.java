package com.eweasel.takehometest.eventbus.multithread;

import com.eweasel.takehometest.eventbus.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class MultiThreadedBusConflationTest {
    private MultiThreadedEventBus bus;
    private TestEventListener busEvents;

    @BeforeEach
    public void setup() {
        bus = new MultiThreadedEventBus();
        busEvents = new TestEventListener(bus.newClient());
    }

    @AfterEach
    public void after() {
        bus.stop();
    }

    @Test
    public void testMultiplePingsSinglePong() {
        new SingleBurstPinger(bus.newClient(), 5);
        new SinglePonger(bus.newConflatingClient());
        bus.startBus();
        busEvents.expectEventually(StreamStartEvent.INSTANCE);
        busEvents.expectEventually(new Ping(1));
        busEvents.expectEventually(new Ping(2));
        busEvents.expectEventually(new Ping(3));
        busEvents.expectEventually(new Ping(4));
        busEvents.expectEventually(new Ping(5));
        busEvents.expectEventually(new Pong(5));
        busEvents.expectNoMore();
    }

}
