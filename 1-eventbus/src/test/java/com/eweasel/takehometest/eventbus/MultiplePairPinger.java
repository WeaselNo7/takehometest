package com.eweasel.takehometest.eventbus;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MultiplePairPinger extends BusNode {
    private final Logger logger = LoggerFactory.getLogger(MultiplePairPinger.class.getName());
    private final long maxPingCount;

    private long pingCount = 0;

    public MultiplePairPinger(EventBus busClient, long maxPingCount) {
        super(busClient);
        this.maxPingCount = maxPingCount;
        busClient.addSubscriber(Ping.class, this::onPing);
        busClient.addSubscriber(Pong.class, this::onPong);
        busClient.startClient();
    }

    @Override
    protected void onStreamStarted() {
        logger.info("onStarted");
        ping();
        ping();
    }

    private void ping() {
        if (pingCount < maxPingCount) {
            pingCount++;
            logger.info("Sending Ping " + pingCount);
            bus.publishEvent(new Ping(pingCount));
        } else {
            logger.info("Max pings sent already");
        }
    }

    private void onPing(EventContext context, Ping ping) {
        logger.info("onPing, saw my own ping: " + ping);
    }

    private void onPong(EventContext context, Pong pong) {
        logger.info("onPong, saw pong: " + pong);
        if (pong.getCounter() % 2 == 0) {
            ping();
            ping();
        }
    }
}
