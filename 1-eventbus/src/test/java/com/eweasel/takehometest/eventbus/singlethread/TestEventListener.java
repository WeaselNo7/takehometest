package com.eweasel.takehometest.eventbus.singlethread;

import com.eweasel.takehometest.eventbus.EventBus;
import com.eweasel.takehometest.eventbus.EventContext;
import com.eweasel.takehometest.eventbus.StreamEvent;

import java.util.LinkedList;
import java.util.Queue;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

public class TestEventListener {
    private final Queue<StreamEvent> events = new LinkedList<>();

    public TestEventListener(EventBus bus) {
        bus.addAllSubscriber(this::onEvent);
    }

    private void onEvent(EventContext context, StreamEvent event) {
        events.add(event);
    }

    public void expect(StreamEvent event) {
        StreamEvent actualEvent = events.peek();
        if (actualEvent == null) {
            fail("Expected " + event + " but no events were on the bus");
        } else {
            events.remove();
            assertEquals(event, actualEvent);
        }
    }

    public void expectNoMore() {
        if (events.peek() != null) {
            fail("Expected empty bus, but found " + events.peek());
        }
    }
}
