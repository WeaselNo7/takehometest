package com.eweasel.takehometest.eventbus;

import java.util.Objects;

public class Ping implements StreamEvent {
    private final long counter;

    public Ping(long counter) {
        this.counter = counter;
    }

    public long getCounter() {
        return counter;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Ping ping = (Ping) o;
        return counter == ping.counter;
    }

    @Override
    public int hashCode() {
        return Objects.hash(counter);
    }

    @Override
    public String toString() {
        return "Ping{" +
                "counter=" + counter +
                '}';
    }
}
