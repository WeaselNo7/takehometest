package com.eweasel.takehometest.eventbus;

import java.util.Objects;

public class Pong implements StreamEvent {
    private final long counter;

    public Pong(long counter) {
        this.counter = counter;
    }

    public long getCounter() {
        return counter;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pong pong = (Pong) o;
        return counter == pong.counter;
    }

    @Override
    public int hashCode() {
        return Objects.hash(counter);
    }

    @Override
    public String toString() {
        return "Pong{" +
                "counter=" + counter +
                '}';
    }
}
