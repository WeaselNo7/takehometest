package com.eweasel.takehometest.eventbus.multithread;

import com.eweasel.takehometest.eventbus.EventBus;
import com.eweasel.takehometest.eventbus.EventContext;
import com.eweasel.takehometest.eventbus.StreamEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

public class TestEventListener {
    private static final Logger logger = LoggerFactory.getLogger(TestEventListener.class.getName());
    private static final long TIMEOUT_MS = 1000;
    private final Queue<StreamEvent> events = new ConcurrentLinkedQueue<>();

    public TestEventListener(EventBus busClient) {
        busClient.addAllSubscriber(this::onEvent);
        busClient.startClient();
    }

    private void onEvent(EventContext context, StreamEvent event) {
        logger.info("onEvent: " + event);
        events.add(event);
    }

    // TODO Tests are fragile because of multithreading.  Should allow a set of events to be passed in here,
    // TODO so where we should, we can avoid explicit ordering of certain events on the stream.
    public void expectEventually(StreamEvent event) {
        final long entryTime = System.currentTimeMillis();
        while (System.currentTimeMillis() < entryTime + TIMEOUT_MS) {
            StreamEvent actualEvent = events.peek();
            if (actualEvent != null) {
                events.remove();
                assertEquals(event, actualEvent);
                return;
            }
        }
        fail("Expected " + event + " but no events were on the bus after " + TIMEOUT_MS + "ms ");
    }

    public void expectNoMore() {
        if (events.peek() != null) {
            fail("Expected empty bus, but found " + events.peek());
        }
    }
}
