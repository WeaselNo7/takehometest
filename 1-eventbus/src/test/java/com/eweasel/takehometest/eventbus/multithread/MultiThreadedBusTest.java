package com.eweasel.takehometest.eventbus.multithread;

import com.eweasel.takehometest.eventbus.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class MultiThreadedBusTest {
    private MultiThreadedEventBus bus;
    private TestEventListener busEvents;

    @BeforeEach
    public void setup() {
        bus = new MultiThreadedEventBus();
        busEvents = new TestEventListener(bus.newClient());
    }

    @AfterEach
    public void after() {
        bus.stop();
    }

    @Test
    public void testSinglePingPong() {
        new SingleBurstPinger(bus.newClient(), 1);
        new SinglePonger(bus.newClient());
        bus.startBus();
        busEvents.expectEventually(StreamStartEvent.INSTANCE);

        busEvents.expectEventually(new Ping(1));
        busEvents.expectEventually(new Pong(1));
        busEvents.expectNoMore();
    }

    @Test
    public void testMultipleSinglePingPong() {
        new MultipleSinglePinger(bus.newClient(), 3);
        new SinglePonger(bus.newClient());
        bus.startBus();
        busEvents.expectEventually(StreamStartEvent.INSTANCE);

        busEvents.expectEventually(new Ping(1));
        busEvents.expectEventually(new Pong(1));
        busEvents.expectEventually(new Ping(2));
        busEvents.expectEventually(new Pong(2));
        busEvents.expectEventually(new Ping(3));
        busEvents.expectEventually(new Pong(3));
        busEvents.expectNoMore();
    }

    @Test
    public void testMultiplePairPingPong() {
        new MultiplePairPinger(bus.newClient(), 6);
        new SinglePonger(bus.newClient());
        bus.startBus();
        busEvents.expectEventually(StreamStartEvent.INSTANCE);

        busEvents.expectEventually(new Ping(1));
        busEvents.expectEventually(new Ping(2));
        busEvents.expectEventually(new Pong(1));
        busEvents.expectEventually(new Pong(2));
        busEvents.expectEventually(new Ping(3));
        busEvents.expectEventually(new Ping(4));
        busEvents.expectEventually(new Pong(3));
        busEvents.expectEventually(new Pong(4));
        busEvents.expectEventually(new Ping(5));
        busEvents.expectEventually(new Ping(6));
        busEvents.expectEventually(new Pong(5));
        busEvents.expectEventually(new Pong(6));
        busEvents.expectNoMore();
    }

    @Test
    public void testMultiplePairPingPongWithLateJoiner() {
        new MultiplePairPinger(bus.newClient(), 6);
        bus.startBus();
        busEvents.expectEventually(StreamStartEvent.INSTANCE);
        busEvents.expectEventually(new Ping(1));
        busEvents.expectEventually(new Ping(2));
        busEvents.expectNoMore();

        // No Pongs until the SinglePonger comes alive and digests the stream
        new SinglePonger(bus.newClient());
        busEvents.expectEventually(new Pong(1));
        busEvents.expectEventually(new Pong(2));
        busEvents.expectEventually(new Ping(3));
        busEvents.expectEventually(new Ping(4));
        busEvents.expectEventually(new Pong(3));
        busEvents.expectEventually(new Pong(4));
        busEvents.expectEventually(new Ping(5));
        busEvents.expectEventually(new Ping(6));
        busEvents.expectEventually(new Pong(5));
        busEvents.expectEventually(new Pong(6));
        busEvents.expectNoMore();
    }

}
