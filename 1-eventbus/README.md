# Question
An event bus is similar to a messaging daemon – you publish messages to it, and subscribers receive those messages.

Write a version of the EventBus designed for single-threaded use (thread calling publishEvent is the same as the thread used for the callback on the
subscriber).

Then write a version of the EventBus which supports multiple producers and multiple consumers (thread calling publishEvent is different to thread making
the callback)

Do not use any non JDK libraries for the multithreading.

Extra points if you can extend the multithreaded version (maybe by extending the interface) so it supports event types where only the latest value matters
(coalescing / conflation) – i.e. multiple market data snapshots arriving while the algo is processing an update.

```
public interface EventBus {
  // Feel free to replace Object with something more specific,
  // but be prepared to justify it
  void publishEvent(Object o);
  
  // How would you denote the subscriber?
  void addSubscriber(Class<?> clazz, ???);
  
  // Would you allow clients to filter the events they receive? How would the interface look like?
  void addSubscriberForFilteredEvents(????);
}
```


# Notes

The constraint of 'use JDK for any concurrency' and the time limit meant that I used naive approaches to thread-safety 
, garbage management and thread responsiveness.

## Garbage
The implementations used here obviously produce a lot of garbage in several ares:
- The wrapping of StreamEvents in DispatcherEvents (to track sequencer timestamp and sequence without 
  polluting the StreamEvent with these sorts of concerns).
- The handover of data across thread boundaries is done with immutable objects that are eventually
  discarded and GCable.
- We are held hostage by the current singleThreadedExecutors management of their own queues of events.

## Performance
While the event bus dispatcher in the multithreaded version is a busy-spin, the threading of the clients is
held hostage by the singleThreadedExecutors used for dispatch to application code.  I'm not totally sure of 
the implementation under-the-hood, but it's not going to be pretty (ThreadPool of '1' I think), and probably
not very tunable.

## Next Steps

As we're staying within the JVM, I'd do the following:
1) Replace my event bus with a RingBuffer (the lmax one), which would provide the following off-the-shelf:
   1) Safe thread management (declaring the strategy for each thread: busy spin, yield, etc)
   2) Non-copying of event data.  Each thread dispatch will safely allow us to have a view onto the ring buffer
      data without needing a copy of the data for thread-safety.
   3) No garbage.  We can use a flyweight to access events on the ringbuffer, with the assurance the data
      is ready for this.  Data in Ringbuffer is mutable, and so not discarded for garbage collection
2) Code generation of the events possible.  Declare events in simple format and generate code off the back of that
   at build time. Giving us:
   1) Safe and consistent mandatory functionality. e.g. keeping toString up-to-date not being the responsibility of 
      a human.  Also event comparison functionality can be generated and extracted to where needed. (we don't
      necessarily want it in the equals/hashcode of the objects themselves, they might only be applicable to the tests)
   2) Safe indices of event types, instead of relying on reflection for Class<?>, which could be error prone.
   3) Can code-generate dispatcher logic too, to use the generated indices and reach into callbacks correctly

# Time taken

Took me **2 hours** (ish) to get the basics in place for the single threaded version (lots of context to swap into my 
head).  

Then a further **2.5 hours** for the multithreaded version.

Then **30 mins** roughly for the conflation 
