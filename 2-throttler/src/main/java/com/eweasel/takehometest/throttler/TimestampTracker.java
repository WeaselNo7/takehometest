package com.eweasel.takehometest.throttler;

public class TimestampTracker {
    private final long[] values;
    private int writeCount = 0;

    public TimestampTracker(int size) {
        if (size <= 0) {
            throw new IllegalArgumentException("Must be size 1 or greater");
        }
        this.values = new long[size];
    }

    private int getNextWritePosition() {
        return writeCount % values.length;
    }

    public long getOldestTimestampIfFull() {
        if (writeCount < values.length) {
            return 0L;
        }
        return values[getNextWritePosition()];
    }

    public void addTimestamp(long timestamp) {
        values[getNextWritePosition()] = timestamp;
        writeCount++;
    }
}
