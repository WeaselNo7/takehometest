package com.eweasel.takehometest.throttler;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.function.Function;
import java.util.function.Supplier;

public class TimeWindowThrottler implements Throttler {
    private final ReadWriteLock lock = new ReentrantReadWriteLock();
    private final ScheduledExecutorService executorService;
    private final TimestampTracker timestampTracker;
    private final Supplier<Long> clock;
    private final Function<StreamEvent, Boolean> eventSender;
    private final int windowDurationMs;

    public TimeWindowThrottler(Supplier<Long> clock, int maxCountInWindow, int windowDurationMs, Function<StreamEvent, Boolean> eventSender) {
        this(clock, maxCountInWindow, windowDurationMs, eventSender, Executors.newScheduledThreadPool(1));
    }

    public TimeWindowThrottler(Supplier<Long> clock, int maxCountInWindow, int windowDurationMs, Function<StreamEvent, Boolean> eventSender, ScheduledExecutorService executorService) {
        this.clock = clock;
        this.windowDurationMs = windowDurationMs;
        this.timestampTracker = new TimestampTracker(maxCountInWindow);
        this.eventSender = eventSender;
        this.executorService = executorService;
    }

    private boolean canSend(long timeSinceOldestTimestampMs) {
        return timeSinceOldestTimestampMs >= windowDurationMs;
    }

    @Override
    public ThrottleResult shouldProceed() {
        final Lock readLock = lock.readLock();
        try {
            readLock.lock();
            final long currentTime = clock.get();
            if (canSend(timeSinceOldestTimestampMs(currentTime))) {
                return ThrottleResult.PROCEED;
            } else {
                return ThrottleResult.DO_NOT_PROCEED;
            }
        } finally {
            readLock.unlock();
        }
    }

    // Will be a large number if tracker isn't full (tracker returns 0 if not full)
    // TODO this sacrifices readability a bit though, I would redo this
    private long timeSinceOldestTimestampMs(long currentTime) {
        return currentTime - timestampTracker.getOldestTimestampIfFull();
    }

    @Override
    public boolean send(StreamEvent event) {
        final Lock writeLock = lock.writeLock();
        try {
            writeLock.lock();
            final long currentTime = clock.get();
            final long timeSinceLastTimestamp = timeSinceOldestTimestampMs(currentTime);
            // If the oldest sent timestamp was outside of our window, attempt to send.
            if (canSend(timeSinceLastTimestamp)) {
                if (eventSender.apply(event)) {
                    timestampTracker.addTimestamp(currentTime);
                    return true;
                }
            }
            return false;
        } finally {
            writeLock.unlock();
        }
    }

    @Override
    public void notifyWhenCanProceed(Runnable callback) {
        final Lock readLock = lock.readLock();
        boolean runCallbackNow = false;
        try {
            readLock.lock();
            final long currentTime = clock.get();
            final long timeSinceEarliestTimestampMs = timeSinceOldestTimestampMs(currentTime);
            // If we can already send the event (things have changed since client last checked),
            // Then call it on this thread straight away.
            if (canSend(timeSinceEarliestTimestampMs)) {
                runCallbackNow = true;
            } else {
                final long timeUntilCallbackMs = windowDurationMs - timeSinceEarliestTimestampMs;
                // Otherwise, setup a timer to callback at the right time
                executorService.schedule(callback, timeUntilCallbackMs, TimeUnit.MILLISECONDS);
            }
        } finally {
            readLock.unlock();
        }
        // Call outside of our lock.
        if (runCallbackNow) {
            callback.run();
        }
    }
}
