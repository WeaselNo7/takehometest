package com.eweasel.takehometest.throttler;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.function.Supplier;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class TimeWindowThrottlerTest {
    private static final long BASE_TIME = 1697979032;
    private Supplier<Long> clock;
    private Function<StreamEvent, Boolean> eventSender;
    private ScheduledExecutorService scheduler;
    private Throttler throttler;

    @SuppressWarnings("unchecked")
    @BeforeEach
    public void setup() {
        clock = Mockito.mock(Supplier.class);
        eventSender = Mockito.mock(Function.class);
        scheduler = Mockito.mock(ScheduledExecutorService.class);
        throttler = new TimeWindowThrottler(clock, 3, 1000, eventSender, scheduler);
    }

    private static long time(long offset) {
        return BASE_TIME + offset;
    }

    private void clock(long time) {
        when(clock.get()).thenReturn(time(time));
    }

    private void successfulSend(StreamEvent event) {
        when(eventSender.apply(event)).thenReturn(true);
        assertTrue(throttler.send(event));
    }

    private void failedSend(StreamEvent event, boolean downStreamSuccess) {
        when(eventSender.apply(event)).thenReturn(downStreamSuccess);
        assertFalse(throttler.send(event));
    }

    @Test
    public void testNothingSent() {
        final Runnable callback = Mockito.mock(Runnable.class);
        final StreamEvent event = Mockito.mock(StreamEvent.class);

        // At time 0, PROCEED, callback straightaway.
        clock(0);
        assertEquals(Throttler.ThrottleResult.PROCEED, throttler.shouldProceed());
        throttler.notifyWhenCanProceed(callback);
        verify(callback, times(1)).run();

        // At time 1000, PROCEED, callback straightaway.
        clock(1000);
        assertEquals(Throttler.ThrottleResult.PROCEED, throttler.shouldProceed());
        throttler.notifyWhenCanProceed(callback);
        verify(callback, times(2)).run();
        verifyNoInteractions(scheduler);

        // Failed send
        failedSend(event, false);
        assertEquals(Throttler.ThrottleResult.PROCEED, throttler.shouldProceed());

        // Successful send, send window is still empty
        successfulSend(event);
        assertEquals(Throttler.ThrottleResult.PROCEED, throttler.shouldProceed());

        verifyNoInteractions(scheduler);
    }

    @Test
    public void testSendUntilFull() {
        final Runnable callback = Mockito.mock(Runnable.class);
        final StreamEvent event = Mockito.mock(StreamEvent.class);

        // Successful send at time 0 and 100
        clock(0);
        successfulSend(event);
        clock(100);
        successfulSend(event);

        // At time 200, PROCEED, callback straightaway.
        clock(200);
        assertEquals(Throttler.ThrottleResult.PROCEED, throttler.shouldProceed());
        throttler.notifyWhenCanProceed(callback);
        verify(callback, times(1)).run();
        verifyNoInteractions(scheduler);

        // Failed downstream send, shouldn't affect moving window
        failedSend(event, false);
        assertEquals(Throttler.ThrottleResult.PROCEED, throttler.shouldProceed());

        // Successful send possible
        successfulSend(event);
        assertEquals(Throttler.ThrottleResult.DO_NOT_PROCEED, throttler.shouldProceed());

        // Successful send not possible (window is full)
        failedSend(event, true);
        assertEquals(Throttler.ThrottleResult.DO_NOT_PROCEED, throttler.shouldProceed());

        // Schedule for next send, won't callback straightaway.
        throttler.notifyWhenCanProceed(callback);
        verify(scheduler).schedule(callback, 800, TimeUnit.MILLISECONDS);
        verifyNoMoreInteractions(callback);
    }

    @Test
    public void testMovingWindow() {
        final Runnable callback = Mockito.mock(Runnable.class);
        final StreamEvent event = Mockito.mock(StreamEvent.class);

        // Successful send at time 0 and 100
        clock(0);
        successfulSend(event);
        clock(100);
        successfulSend(event);
        clock(200);
        successfulSend(event);
        assertEquals(Throttler.ThrottleResult.DO_NOT_PROCEED, throttler.shouldProceed());

        // Still inside moving window
        clock(300);
        failedSend(event, true);
        assertEquals(Throttler.ThrottleResult.DO_NOT_PROCEED, throttler.shouldProceed());

        // first event is now outside moving window of 1000, successful send now possible
        clock(1000);
        assertEquals(Throttler.ThrottleResult.PROCEED, throttler.shouldProceed());
        successfulSend(event);
        assertEquals(Throttler.ThrottleResult.DO_NOT_PROCEED, throttler.shouldProceed());

        // other original events are now outside of moving window
        clock(1200);
        assertEquals(Throttler.ThrottleResult.PROCEED, throttler.shouldProceed());
        successfulSend(event);
        successfulSend(event);
        assertEquals(Throttler.ThrottleResult.DO_NOT_PROCEED, throttler.shouldProceed());

        // Schedule for next send, won't callback straightaway.
        clock(1500);
        assertEquals(Throttler.ThrottleResult.DO_NOT_PROCEED, throttler.shouldProceed());
        throttler.notifyWhenCanProceed(callback);
        verify(scheduler).schedule(callback, 500, TimeUnit.MILLISECONDS);
        verifyNoMoreInteractions(callback);
    }

}
