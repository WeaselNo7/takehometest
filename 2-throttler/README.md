# Question

A Throttler allows a client to restrict how many times something can happen in a given time period (for example we may not want to send more than a
number of quotes to an exchange in a specific time period).

It should be possible to ask it if we can proceed with the throttled operation, as well as be notified by it.
Do not assume thread safety in the subscriber.

```
public interface Throttler {
  // check if we can proceed (poll)
  ThrottleResult shouldProceed();

  // subscribe to be told when we can proceed (Push)
  notifyWhenCanProceed(???);

  enum ThrottleResult {
    PROCEED, // publish, aggregate etc
    DO_NOT_PROCEED //
  }
}
```

Also, highly desirable that throttle be rolling time-window based.

# Notes

I would actually implement this in the event bus solution.  Timing signal driven from events on the stream (including 
an extra TimeEvent published periodically for determinism).  This implementation would be thread-safe due to being 
isolated to being uses on an event handling thread.

However the question here explicitly states lack of thread-safety, so I instead make a general purpose solution.

On the implementation I did, the following is of note:
- Because we don't want race conditions on the 'notifyWhenCanProceed', we lock aggressively
- Similarly with race conditions in the 'send' method. The only way to handle this really was to 
  allow the Throttler to arbitrate between the calling code and the event sender itself.

# Time taken

Took me **2 hours** (roughly) for the throttler.
