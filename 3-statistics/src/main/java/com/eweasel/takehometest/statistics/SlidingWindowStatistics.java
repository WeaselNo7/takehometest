package com.eweasel.takehometest.statistics;

public interface SlidingWindowStatistics {
    void add(int measurement);

    // subscriber will have a callback that'll deliver a Statistics instance (push)
    // Instead of this, as I'm trying for zero GC.  My callback will allow the consumer to just use the polling
    // method to copy the latest data.
    void subscribeForStatistics(Runnable callback);

    // get latest statistics (poll)
    void getLatestStatistics(SlidingWindowStatisticsImpl.Statistics statistics);

}