package com.eweasel.takehometest.statistics;

import it.unimi.dsi.fastutil.ints.Int2DoubleArrayMap;
import it.unimi.dsi.fastutil.ints.Int2IntOpenHashMap;
import it.unimi.dsi.fastutil.ints.IntArrayList;
import it.unimi.dsi.fastutil.longs.LongArrayList;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.function.Supplier;

public class SlidingWindowStatisticsImpl implements SlidingWindowStatistics {
    // If we get this right, it reduces any garbage from fastutil
    private static final int EXPECTED_MAX = 1024;
    private final ReadWriteLock lock = new ReentrantReadWriteLock();
    
    // TODO a linked-list version of these, for speed
    private final LongArrayList timestamps = new LongArrayList(EXPECTED_MAX);
    private final IntArrayList values = new IntArrayList(EXPECTED_MAX);
    
    private final Supplier<Long> clock;
    private final int windowDurationMs;

    private final MeanCalculator meanCalculator = new MeanCalculator();
    private final ModeCalculator modeCalculator = new ModeCalculator(); 
    private final PercentileCalculator percentileCalculator = new PercentileCalculator();
    private final Calculator[] calculators = new Calculator[] { meanCalculator, modeCalculator, percentileCalculator };

    private final List<Runnable> consumers = new ArrayList<>();

    public SlidingWindowStatisticsImpl(Supplier<Long> clock, int windowDurationMs) {
        this.clock = clock;
        this.windowDurationMs = windowDurationMs;
    }

    @Override
    public void add(int measurement) {
        final long currentTime = clock.get();
        final Lock writeLock = lock.writeLock();
        try {
            writeLock.lock();
            
            removeAnyValuesPastThreshold(currentTime);
            
            timestamps.add(currentTime);
            values.add(measurement);
            for (Calculator calculator : calculators) {
                calculator.addValue(measurement);
            }
            
        } finally {
            writeLock.unlock();
        }

        // TODO do we really want the calling thread to be responsible for dispatching callbacks?
        for (Runnable callback : consumers) {
            callback.run();
        }
    }
    
    private boolean needToRemoveValues(long currentTime) {
        if (timestamps.isEmpty()) {
            return false;
        }
        return isOlderThanThreshold(timestamps.getLong(0), currentTime);
    }

    private void removeAnyValuesPastThreshold(long currentTime) {
        while (!timestamps.isEmpty()) {
            if (isOlderThanThreshold(timestamps.getLong(0), currentTime)) {
                timestamps.removeLong(0);
                int value = values.removeInt(0);
                for (Calculator calculator : calculators) {
                    calculator.removeValue(value);
                }
            } else {
                break;
            }
        }
    }

    private boolean isOlderThanThreshold(long timestamp, long currentTime) {
        final long threshold = currentTime - windowDurationMs;
        return (timestamp <= threshold);
    }

    @Override
    public void subscribeForStatistics(Runnable callback) {
        consumers.add(callback);
    }

    @Override
    public void getLatestStatistics(Statistics statistics) {
        final long currentTime = clock.get();

        Lock currentLock = lock.readLock();
        try {
            currentLock.lock();
            if (needToRemoveValues(currentTime)) {
                // Need to upgrade our lock to a writelock!
                currentLock.unlock();

                // This will be unlocked by the outer try-finally
                currentLock = lock.writeLock();
                currentLock.lock();

                removeAnyValuesPastThreshold(currentTime);
            }

            // By the time we get here, our data is in good shape
            statistics.reevaluate(meanCalculator, modeCalculator, percentileCalculator);

        } finally {
            currentLock.unlock();
        }
    }

    private interface Calculator {
        void addValue(int value);
        void removeValue(int value);
    }

    public static class Statistics {
        private final int[] percentilesSupported;
        private final Int2DoubleArrayMap percentiles = new Int2DoubleArrayMap();
        private double mean;
        private int mode;

        public Statistics(int... percentilesSupported) {
            this.percentilesSupported = percentilesSupported;
        }

        private void reevaluate(MeanCalculator meanCalculator, ModeCalculator modeCalculator, PercentileCalculator percentileCalculator) {
            mean = meanCalculator.getMean();
            mode = modeCalculator.getMode();
            for (int percentile : percentilesSupported) {
                percentiles.put(percentile, percentileCalculator.getPercentile(percentile));
            }
        }

        public double getMean() {
            return mean;
        }

        public int getMode() {
            return mode;
        }

        public double getPctile(int pctile) {
            return percentiles.get(pctile);
        }
    }


    private static class MeanCalculator implements Calculator {
        private int valueCount = 0;
        private double currentValue = Double.NaN;

        @Override
        public void addValue(int value) {
            if (valueCount <= 0) {
                valueCount = 1;
                currentValue = value;
            } else {
                double oldValue = currentValue;
                valueCount++;
                currentValue = oldValue + ((value - oldValue) / valueCount);
            }
        }

        @Override
        public void removeValue(int value) {
            if (valueCount <= 1) {
                valueCount = 0;
                currentValue = Double.NaN;
            } else {
                double oldValue = currentValue;
                valueCount--;
                currentValue = ((oldValue * (valueCount + 1)) - value) / valueCount;
            }
        }

        public double getMean() {
            return currentValue;
        }
    }
    
    private static class ModeCalculator implements Calculator {
        private final Int2IntOpenHashMap valueCountMap = new Int2IntOpenHashMap(EXPECTED_MAX);

        private int currentValue = 0;

        @Override
        public void addValue(int value) {
            valueCountMap.addTo(value, 1);
            // If there's only one entry, that's our result
            if (valueCountMap.size() == 1) {
                currentValue = value;
            // Otherwise, if we're increasing the current best, that's our result
            } else {
                if (currentValue != value) {
                    // Otherwise, compare the currentValue's count and the new addition's count and choose the highest
                    int currentValueCount = valueCountMap.get(currentValue);
                    int newValueCount = valueCountMap.get(value);
                    if (newValueCount > currentValueCount) {
                        currentValue = value;
                    }
                }
            }
        }

        @Override
        public void removeValue(int value) {
            int previousCount = valueCountMap.addTo(value, -1);
            if (previousCount <= 1) {
                valueCountMap.remove(value);
            }

            // TODO We don't have many choices but to iterate the whole lot to get our best.
            // TODO Would could use some custom red-black tree that is sorted by value (count) instead of key.
            // TODO That way we can make changes to our dataset and it's reorganised in log(n) time.
            int bestKey = 0;
            int bestCount = 0;
            for (int key : valueCountMap.keySet()) {
                int count = valueCountMap.get(key);
                if (count > bestCount) {
                    bestKey = key;
                    bestCount = count;
                }
            }
            currentValue = bestKey;
        }

        public int getMode() {
            return currentValue;
        }
    }

    private static class PercentileCalculator implements Calculator {
        // TODO a linked-list version of this, for speed
        private final IntArrayList values = new IntArrayList(EXPECTED_MAX);

        @Override
        public void addValue(int value) {
            int location = binarySearch(value, true);
            values.add(location, value);
        }

        @Override
        public void removeValue(int value) {
            int location = binarySearch(value, false);
            values.removeInt(location);
        }

        private int binarySearch(int value, boolean hypothetical) {
            int first = 0;
            int last = values.size() - 1;
            int mid = (first + last)/2;
            while (first <= last){
                final int midValue = values.getInt(mid);
                if (midValue < value) {
                    first = mid + 1;
                } else if (midValue == value) {
                    return mid;
                } else {
                    last = mid - 1;
                }
                mid = (first + last)/2;
            }
            // TODO deal with error case in calling code
            if (!hypothetical) {
                return -1;
            }
            return first;
        }

        public double getPercentile(int percentile) {
            if (values.size() == 1) {
                return values.getInt(0);
            }
            double position = ((values.size() - 1) * percentile) / 100.0;
            int beforePosition = (int)position;
            int afterPosition = (int)Math.ceil(position);
            double ratio = position - beforePosition;
            
            int valueBefore = values.getInt(beforePosition);
            int valueAfter = values.getInt(afterPosition);
            return ((1 - ratio) * valueBefore) + (ratio * valueAfter);
        }
    }
}
