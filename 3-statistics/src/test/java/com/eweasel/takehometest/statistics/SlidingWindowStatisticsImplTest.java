package com.eweasel.takehometest.statistics;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.function.Supplier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

public class SlidingWindowStatisticsImplTest {
    private static final long BASE_TIME = 1697979032;

    private final SlidingWindowStatisticsImpl.Statistics result = new SlidingWindowStatisticsImpl.Statistics(25, 50, 75);
    private SlidingWindowStatistics statistics;
    private Supplier<Long> clock;

    @SuppressWarnings("unchecked")
    @BeforeEach
    public void setup() {
        clock = Mockito.mock(Supplier.class);
        statistics = new SlidingWindowStatisticsImpl(clock, 1000);
    }

    private static long time(long offset) {
        return BASE_TIME + offset;
    }

    private void clock(long time) {
        when(clock.get()).thenReturn(time(time));
    }

    private void expectValues(double mean, int mode, double percentile25, double percentile50, double percentile75) {
        statistics.getLatestStatistics(result);
        assertEquals(mean, result.getMean());
        assertEquals(mode, result.getMode());
        assertEquals(percentile25, result.getPctile(25));
        assertEquals(percentile50, result.getPctile(50));
        assertEquals(percentile75, result.getPctile(75));
    }

    @Test
    public void basicTest() {
        clock(0);
        statistics.add(1000);
        statistics.add(2000);

        expectValues(1500.0, 1000, 1250.0, 1500.0, 1750.0);
    }

    @Test
    public void testWindow() {
        clock(0);
        statistics.add(1000);
        statistics.add(2000);
        statistics.add(3000);

        clock(500);
        statistics.add(4000);
        statistics.add(5000);

        clock(950);
        statistics.add(3000);

        expectValues(3000.0, 3000, 2250.0, 3000.0, 3750.0);

        // First set of numbers fall out of window
        clock(1050);
        expectValues(4000.0, 5000, 3500.0, 4000.0, 4500.0);

        // Add another
        statistics.add(3000);
        expectValues(3750.0, 3000, 3000.0, 3500.0, 4250.0);
    }
}
