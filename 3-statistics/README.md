# Question

Write a general purpose class that calculates statistics about a sequence of integers.

An example use case would be latency statistics – you can add latency measurements to it and get statistics out.

Some clients may only want to be notified when the statistics match certain criteria – e.g. the mean has gone above a threshold.

Do not assume that subscribers will be threadsafe. The add method may be called by a different thread than the callback should be made on.

```
public interface SlidingWindowStatistics {
  void add(?? measurement);
  
  // subscriber will have a callback that'll deliver a Statistics instance (push)
  void subscribeForStatistics(???);

  // get latest statistics (poll)
  Statistics getLatestStatistics();
  
  public interface Statistics {
    ?? getMean();
    ?? getMode();
    ?? getPctile(int pctile);
  }
}
```

# Notes

I made the assumption here that the 'moving window' referred to is a time-based one,
and the values given via the interface are the ones to have stats created for.

Choices I had to make:
- Do I use arrays of values to make iterating and 'doing maths' faster, and more hospitable for libraries
  and perhaps even vector functions?  
- Or do I have a collection of elements that can expand and contract, but sacrifice the data locality?

I went with the garbage free option, because I was starting to get sick of producing code that generates so much
garbage.

As a result, the class is a little complex, and I had to change the interface dramatically, to force consumers to 
copy data from our class, rather than pushing immutable objects (garbage).

To answer the extra question: "how would you allow consumers to only be notified on certain conditions?",  I would 
allow them to subscribe with a Predicate object that I would test before calling the Runnable.  That obviously
comes with some concerns about thread-safety, if users are allowed a free-reign on their implementation. 

# Time taken

**3.5 hours** . I stopped at this point, as it was too time consuming!

