# Take Home Test V2 
Sam Green **(sam.green@e-weasel.co.uk)**

See [Event bus exercise README](1-eventbus/README.md)

See [Throttler exercise README](2-throttler/README.md)

See [Statistics exercise README](3-statistics/README.md)